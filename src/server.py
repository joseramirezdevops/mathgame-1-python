# -*- coding: utf-8 -*-

# 0. import libraries
from flask import Flask, render_template
from flask import request # used for getting values

# 1.Flask serving static HTML file in this path
app = Flask(__name__, template_folder='./frontend')#, static_folder='./static/') # instance of the Flask class


print('Running template in the following path: '+ app.template_folder)
print('Running static files in the following path: '+ app.static_folder)

#app.static_folder('frontend')

app.debug=True

# 2. Indicate the file to 

@app.route("/")
def hi():
    return render_template("index.html")

@app.route("/", methods=['GET', 'POST'])
def getValue1():
    # for POST method we have:
    # request.method and request.form
    if request.method == 'POST': # if the method is 'POST' then I will produce something
        if "nsubmit2Html" in request.form:
            value1= request.form['naddNumber1Box']
            value1= int(value1)
            print('value1=',value1)
            
            value2= request.form['naddNumber2Box']
            value2= int(value2)
            print('value2=',value2)
            
            add1= value1 + value2
            
            value3= request.form['naddtSudentResultBox']
            value3= int(value3)
            print('value3=',value3)
            
            #verification
            if (value3==add1):
                print('Correct=', add1)
                return render_template('index.html', number1Py=value1,
                                       number2Py=str(value2), number3Py=str(value3),
                                       number4Py=str(add1), text5Py='Correct!' )
            else:
                print('Wrong. expected value is:', str(value1), '+', str(value2), '=', str(add1))
                print('But received= ', value3)
            
                return render_template('index.html', number1Py=str(value1),
                                       number2Py=str(value2), number3Py=str(value3),
                                       number4Py=str(add1), text5Py='Something is wrong. :( please try again!' )
        elif "nsubmit3Html" in request.form:
            return render_template("index.html")
        else:
            None




# 3. run app (Specify ip and port)
if __name__=='__main__':
    app.run(host="127.0.0.1", port="4001")  # for Windows Jose 3333
    
